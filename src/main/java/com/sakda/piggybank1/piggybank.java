/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.piggybank1;

/**
 *
 * @author MSI GAMING
 */
public class piggybank {

    String name; // สร้างตัวแปร name ชนิด String เอาไว้เก็บชื่อ
    private int count; // สร้างตัวแปร count ชนิด int เอาไว้ นับจำนวนเหรียญ
    double coin; // สร้างตัวแปร coin ชนิด double เอาไว้ เก็บจำนวนเงิน
    private int coinType; // สร้างตัวแปร coinType ชนิด int เอาไว้ แบ่งประเภทเหรียญ
    double coin1 = 1; // สร้างตัวแปร coin1 ชนิด double มีค่า = 1 เอาไว้ แทนเป็นเหรียญ1บาท
    double coin2 = 2; // สร้างตัวแปร coin2 ชนิด double มีค่า = 2 เอาไว้ แทนเป็นเหรียญ 2 บาท
    double coin5 = 5; // สร้างตัวแปร coin5 ชนิด double มีค่า = 5 เอาไว้ แทนเป็นเหรียญ 5 บาท
    double coin10 = 10; // สร้างตัวแปร coin10 ชนิด double มีค่า = 10 เอาไว้ แทนเป็นเหรียญ 10 บาท

    public piggybank(String name, double coin) {
        this.name = name;
        this.coin = coin;
    }

    public int getCount() {
        return count;
    }

    public double getCoin1() {
        return coin1;
    }

    public double getCoin2() {
        return coin2;
    }

    public double getCoin5() {
        return coin5;
    }

    public double getCoin10() {
        return coin10;
    }

    public void setInput(int count, int coinType) {
        this.count = count;
        this.coinType = coinType;
        if (coinType == 1) {
            this.coin += count * coin1;
        }else if(coinType == 2){
            this.coin += count * coin2;
        }else if(coinType == 5){
            this.coin += count * coin5;
        }else if(coinType == 10){
            this.coin += count * coin10;
        }else{
            System.out.println("Error!!! TypeCoin Erroneous ");
        }
    }

}
