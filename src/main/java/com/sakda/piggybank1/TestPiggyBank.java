/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.piggybank1;

/**
 *
 * @author MSI GAMING
 */
public class TestPiggyBank { // สร้าง class TestPiggyBank
    public static void main(String[] args) {
        piggybank piggybank1 = new piggybank ("Sakda",0); //piggybank1 ถูกสร้างขึ้น ( name , coin)
        
        piggybank1.setInput(20, 1);// เรียกใช้ method setInput ( 20 คือ count(จำนวนเหรียญ) , 1 คือ coinType(ประเภทของเหรียญ))
        System.out.println(piggybank1.name +" "+piggybank1.coin1+" baht coin savings amount "+piggybank1.getCount() +" coins is the total number "+piggybank1.coin +" baht");
        //แสดงผล ชื่อ ประเภทเหรียญ จำนวนเหรียญ จำนวนเงิน
        
         piggybank1.setInput(10, 2);// เรียกใช้ method setInput ( 10 คือ count(จำนวนเหรียญ) , 2 คือ coinType(ประเภทของเหรียญ))
        System.out.println(piggybank1.name +" "+piggybank1.coin2+" baht coin savings amount "+piggybank1.getCount() +" coins is the total number "+piggybank1.coin +" baht");
        //แสดงผล ชื่อ ประเภทเหรียญ จำนวนเหรียญ จำนวนเงิน
        
         piggybank1.setInput(15, 5);// เรียกใช้ method setInput ( 15 คือ count(จำนวนเหรียญ) , 5 คือ coinType(ประเภทของเหรียญ))
        System.out.println(piggybank1.name +" "+piggybank1.coin5+" baht coin savings amount "+piggybank1.getCount() +" coins is the total number "+piggybank1.coin +" baht");
        //แสดงผล ชื่อ ประเภทเหรียญ จำนวนเหรียญ จำนวนเงิน
        
        piggybank1.setInput(25, 10);// เรียกใช้ method setInput ( 25 คือ count(จำนวนเหรียญ) , 10 คือ coinType(ประเภทของเหรียญ))
        System.out.println(piggybank1.name +" "+piggybank1.coin10+" baht coin savings amount "+piggybank1.getCount() +" coins is the total number "+piggybank1.coin +" baht");
        //แสดงผล ชื่อ ประเภทเหรียญ จำนวนเหรียญ จำนวนเงิน
        
        piggybank1.setInput(25, 20);// เรียกใช้ method setInput ( 25 คือ count(จำนวนเหรียญ) , 20 คือ coinType(ประเภทของเหรียญ))
        
        System.out.println(piggybank1.name +" will have all the money in the jar "+piggybank1.coin);
        //แสดงผล ชื่อและจำนวนเงินทั้งหมด
    }
}
